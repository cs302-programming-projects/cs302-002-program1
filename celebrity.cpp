#include "celebrity.h"

/*
   Brian Le
   This file contains the implementation of the celebrity class's member functions
   */

Celebrity::Celebrity(): celebrity_name(nullptr), availability('x')
{
//	availability = 'x'; // x just means no availability
}

Celebrity::Celebrity(const Celebrity & to_copy) // copy constructor
{
	copy_all(to_copy);	// deep copy function
}

Celebrity::Celebrity(char * name, string & works, char day): celebrity_name(nullptr), availability('x')
{
	if (!celebrity_name)
	{
		celebrity_name = new char[strlen(name) + 1];
		strcpy(celebrity_name, name);
	}

	celebrity_works = works;
	availability = day;
}

Celebrity::~Celebrity()
{
	delete[] celebrity_name;
}

Celebrity & Celebrity::operator=(const Celebrity & src)
{
	// doing a deep copy.
	if (&src == this)
		return *this;

	if (celebrity_name)
		delete [] celebrity_name;

	copy_all(src);

	return *this;

	/*
	if (!celebrity_name)
	{
		celebrity_name = new char[strlen(src.celebrity_name) + 1];
		strcpy(celebrity_name, src.celebrity_name);
	}

	celebrity_works = src.celebrity_works;
	availability = src.availability;

	fans_name = src.fans_name;
	fans_phone = src.fans_phone;
	return *this;
	*/
}


bool Celebrity::display_celebrity()
{
	if (celebrity_name)
		cout << "\nName: " << celebrity_name << endl;

	cout << "Works: " << celebrity_works << endl;

	cout << "Availability: ";

	return dayoftheweek(availability);
}

bool Celebrity::dayoftheweek(char x)
{
	switch (x)
	{
		case 'U':
			cout << "Sunday\n";
			break;
		case 'M':
			cout << "Monday\n";
			break;
		case 'T':
			cout << "Tuesday\n";
			break;
		case 'W':
			cout << "Wednesday\n";
			break;
		case 'R':
			cout << "Thursday\n";
			break;
		case 'F':
			cout << "Friday\n";
			break;
		case 'S':
			cout << "Saturday\n";
			break;
		default:
			cout << "N/A\n";
			break;
	}

	return true;

}


bool Celebrity::find_reservation(string & user_name, long phone)
{
	if (compare_user(user_name, phone))
	{
		cout << '\n';
		cout << "We found your reservation with " << celebrity_name << " on ";
		dayoftheweek(availability);
		display_celebrity();
		cout << "\n\n";

		return true;
	}

	return false;

	/*
	for (int i = 0; i < fans_name.size(); ++i)
	{
		if (fans_name[i] == user_name && fans_phone[i] == phone)
		{
			return true;
		}
	}

	return false;
	*/
}

bool Celebrity::compare_user(const string & user_name, long phone) // used my find_reservation
{
	int size = fans_name.size();
	for (int i = 0; i < size; ++i)
	{
		if (fans_name[i] == user_name && fans_phone[i] == phone)
		{
			return true;
		}
	}

	return false;
}

bool Celebrity::check_availability(char c)
{
	if (c == availability)
	{
		cout << '\n';
		cout << celebrity_name << " is availabe on ";
		dayoftheweek(c);
		display_celebrity();
		cout << "\n\n";
		return true;
	}

	return false;
}

bool Celebrity::schedule_reservation(const string & user_name, long phone_number)
{
	fans_name.push_back(user_name);
	fans_phone.push_back(phone_number);
	cout << "Your Reservation with " << celebrity_name << " has been created.\n";
	return true;
}

bool Celebrity::cancel_reservation(const string & user_name, long phone_number)
{
	int size = fans_name.size();

	for (int i = 0; size; ++i)
	{
		if (fans_name[i] == user_name && fans_phone[i] == phone_number)
		{
			fans_name.erase(fans_name.begin() + i);
			fans_phone.erase(fans_phone.begin() + i);
			cout << "Your Reservation with " << celebrity_name << " has been cancelled.\n";
			return true;
		}
	}

	return false;
}

bool Celebrity::compare_celebrity(char * celeb_name)
{
	if (strcmp(celeb_name, celebrity_name) == 0)
		return true;
	else
		return false;
}

bool Celebrity::copy_name(char * string)
{
	if (!celebrity_name)
	{
		celebrity_name = new char[strlen(string) + 1];
		strcpy(celebrity_name, string);
		return true;
	}

	return false;
}

bool Celebrity::copy_all(const Celebrity & src) // basically the deep copy
{
	if (src.celebrity_name && !celebrity_name)
	{
		celebrity_name = new char[strlen(src.celebrity_name) + 1];
		strcpy(celebrity_name, src.celebrity_name);
	}

	celebrity_works = src.celebrity_works;
	availability = src.availability;

	return true;
}



