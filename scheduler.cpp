#include "scheduler.h"

/* 
   Brian Le

   This file contains all the member-function implementations from the Writer, Actor, and Artist Scheduler classes
   */

// Actor Scheduler
ActorScheduler::ActorScheduler(): actor_list(nullptr)
{
}

ActorScheduler::ActorScheduler(const ActorScheduler & to_copy): actor_list(nullptr)
{
	// perform deep copy
	if (!to_copy.actor_list)
		actor_list = nullptr;

	deep_copy2(to_copy);
}

ActorScheduler & ActorScheduler::operator=(const ActorScheduler & src)
{
	if (&src != this)
	{
		this->remove_all();
		deep_copy2(src);
	}

	return *this;
}

int ActorScheduler::deep_copy2(const ActorScheduler & to_copy)
{
	if (!to_copy.actor_list)
		return 0;

	ActorNode * head = nullptr;
	return deep_copy2(head, to_copy.actor_list, to_copy.actor_list->get_next());
}

int ActorScheduler::deep_copy2(ActorNode *& head, ActorNode * copy_rear, ActorNode * copy_head)
{
	int i = 0;

	if (!copy_head) // if we somehow reach null
		return 0;

	head = new ActorNode(*copy_head);

	if (copy_head == copy_rear) // if we reached the end or if there is only one node from src.
	{
		this->actor_list = head;
		this->actor_list->set_next(actor_list);
		return ++i;
	}
	
	i = 1 + deep_copy2(head->get_next(), copy_rear, copy_head->get_next());
	this->actor_list->set_next(head); // while unwinding, connect the rear back to head.
	return i;
}


int ActorScheduler::deep_copy(const ActorScheduler & to_copy)
{
	if (!to_copy.actor_list)
		return 0;

	ActorNode * traverse = actor_list;
	return deep_copy(actor_list, traverse, to_copy.actor_list, to_copy.actor_list->get_next());
}

int ActorScheduler::deep_copy(ActorNode *& rear, ActorNode *& head, ActorNode * copy_rear, ActorNode * copy_head)
{
	int i = 0;
	if (copy_head == nullptr)
		return 0;

	head = new ActorNode(*copy_head);

	if (copy_head == copy_rear) // at the rear
	{
		rear = head;
		rear->set_next(rear);
		return ++i;
	}


	i = 1 + deep_copy(rear, head->get_next(), copy_rear, copy_head->get_next()); 

	rear->set_next(head);
	return i;
}


ActorScheduler::~ActorScheduler()
{
	// deallocation code goes here.

	remove_all();
}

bool ActorScheduler::find_reservation(string & username, long phone)
{	
	if (!actor_list)
		return false;

	return find_reservation(actor_list->get_next(), actor_list, username, phone);
}

bool ActorScheduler::find_reservation(ActorNode * head, ActorNode * rear, string & username, long phone)
{
	if (head == rear)
	{
		return head->find_reservation(username, phone);
	}

	head->find_reservation(username, phone);

	return find_reservation(head->get_next(), rear, username, phone);
}

bool ActorScheduler::make_reservation(string & username, long phone, char * celebrity_name)
{
	if (!actor_list)
		return false;

	ActorNode * current = actor_list->get_next();

	return make_reservation(current, actor_list, username, phone, celebrity_name);
}

bool ActorScheduler::make_reservation(ActorNode *& head, ActorNode * rear, string & username, long phone, char * celebrity_name)
{
	if (head == rear && head->compare_celebrity(celebrity_name) == false)
		return false;

	if (head->compare_celebrity(celebrity_name))
	{
		head->schedule_selfie(username, phone);
		return true;
	}

	return make_reservation(head->get_next(), rear, username, phone, celebrity_name);
}


bool ActorScheduler::cancel_reservation(string & username, long phone, char * celebrity_name)
{
	if (!actor_list)
		return false;

	return cancel_reservation(actor_list->get_next(), actor_list, username, phone, celebrity_name);
}

bool ActorScheduler::cancel_reservation(ActorNode *& head, ActorNode * rear, string & username, long phone, char * celebrity_name)
{
	if (head == rear && !head->compare_celebrity(celebrity_name) && !head->compare_user(username, phone))
	{
		return false; // reservation not found
	}

	if (head->compare_celebrity(celebrity_name) && head->compare_user(username, phone))
	{
		return head->cancel_selfie(username, phone); 
	}

	return cancel_reservation(head->get_next(), rear, username, phone, celebrity_name);
}


bool ActorScheduler::add_actor(char * character, float price, char * celeb_name, string & celeb_works, char availability)
{
	// the "rear pointer" is the actor_list.

	if (!actor_list) // no actors in list.
	{
		actor_list = new ActorNode(character, price, celeb_name, celeb_works, availability);
		actor_list->set_next(actor_list);
		return true;
	}

	// If there is:
	ActorNode * temp = new ActorNode(character, price, celeb_name, celeb_works, availability);
	temp->set_next(actor_list->get_next());
	actor_list->set_next(temp);
	actor_list = actor_list->get_next();
	return true;
}


int ActorScheduler::display_actors()
{
	if (!actor_list)
		return 0;
	
	return display_actors(actor_list->get_next(), actor_list);
}

int ActorScheduler::display_actors(ActorNode * head, ActorNode * rear)
{
	int i = 0;

	if (head == rear) // at the end
	{
		head->display_celebrity();
		head->display_actor_info();
		return 1;
	}

	head->display_celebrity();
	head->display_actor_info();

	i = 1 + display_actors(head->get_next(), rear);

	return i;
}


int ActorScheduler::remove_all()
{
	if (!actor_list) // empty
		return 0;

	ActorNode * current = actor_list->get_next();
	actor_list->set_next(nullptr);

	return remove_all(current);
}

int ActorScheduler::remove_all(ActorNode *& head)
{
	int i = 0;

	if (!head)
		return 1;

	i = 1 + remove_all(head->get_next());

	delete head;

	return i;
}

int ActorScheduler::find_availability(char x) const
{
	if (!actor_list)
		return false;

	return find_availability(actor_list->get_next(), actor_list, x);
}

int ActorScheduler::find_availability(ActorNode * head, ActorNode * rear, char x) const
{
	int i = 0;

	if (head == rear)
	{
		if (head->check_availability(x))
			return ++i;

		return 0;
	}

	if (head->check_availability(x))
		++i;


	i = i + find_availability(head->get_next(), rear, x);

	return i;
}


// Artist Scheduler
ArtistScheduler::ArtistScheduler()
{
	int size = 26; // 26 letters in the alphabet
	artist_list = new ArtistNode*[size];	

	for (int i = 0; i < size; ++i)
	{
		artist_list[i] = nullptr;
	}
}

ArtistScheduler::ArtistScheduler(const ArtistScheduler & to_copy)
{
	// perform deep copy
	int size = 26; // 26 letters in the alphabet
	artist_list = new ArtistNode*[size];	

	for (int i = 0; i < size; ++i)
	{
		// for each array of artists, recursively copy like a linked list
		if (to_copy.artist_list[i])
			deep_copy(artist_list[i], to_copy.artist_list[i]);
	}

}

ArtistScheduler & ArtistScheduler::operator=(const ArtistScheduler & src)
{
	if (&src != this)
	{
		// deallocate and call deepcopy
	}

	return *this;
}

int ArtistScheduler::deep_copy(ArtistNode *& head, ArtistNode * to_copy)
{
	int i = 0;

	if (!to_copy) // at the end
		return 1;

	head = new ArtistNode(*to_copy);

	i = 1 + deep_copy(head->get_next(), to_copy->get_next());

	return i;
}

ArtistScheduler::~ArtistScheduler()
{
	// deallocate
	remove_all();
}

int ArtistScheduler::get_key(char * celeb_name) const
{
	// getting ascii values.

	if (!celeb_name)
		return -1; 

	int key = 0;
	char first_initial = tolower(celeb_name[0]);

	key = first_initial - 97;

	if (key > 25 || key < 0) // something went wrong in getting the keys 
		return -1; 
	else
		return key;

}


bool ArtistScheduler::find_reservation(string & username, long phone)
{	
	if (!artist_list)
		return false;
	
	for (int i = 0; i < 26; ++i)
	{
		if (find_reservation(artist_list[i], username, phone))
			return true;
	}

	return false;

}

bool ArtistScheduler::find_reservation(ArtistNode * head, string & username, long phone)
{

	if (!head) // at the end
		return false;

	head->Celebrity::find_reservation(username, phone);

	return find_reservation(head->get_next(), username, phone);
}


bool ArtistScheduler::make_reservation(string & username, long phone, char * celebrity_name)
{
	int index = get_key(celebrity_name);

	if(!artist_list)
	{
		cout << "There are currently no Artists.\n";
		return false;
	}

	if (index < 0 || index > 25)
	{
		cout << "Unable to locate Artist. Please try again.\n";
		return false;
	}

	if (!artist_list[index])
	{
		cout << "Artist not found. Please try again.\n";
		return false;
	}
	
	for (int i = 0; i < 26; ++i)
	{
		if(make_reservation(artist_list[i], username, phone, celebrity_name))
			return true;
	}

	return false;
}

bool ArtistScheduler::make_reservation(ArtistNode *& head, string & username, long phone, char * celebrity_name)
{
	if (!head)
		return false;

	if (head->compare_celebrity(celebrity_name))
	{
		head->schedule_artistmeet(username, phone);
		return true;
	}

	return make_reservation(head->get_next(), username, phone, celebrity_name);
}

bool ArtistScheduler::cancel_reservation(const string & username, long phone, char * celebrity_name)
{
	if (!artist_list) // no artist
		return false;

	for (int i = 0; i < 26; ++i)
	{
		if (cancel_reservation(artist_list[i], username, phone, celebrity_name))
			return true;
	}

	return false;
}

bool ArtistScheduler::cancel_reservation(ArtistNode *& head, const string & username, long phone, char * celeb_name)
{
	if (!head) // at the end
		return false;

	if (head->compare_celebrity(celeb_name))
	{
		return head->cancel_artistmeet(username, phone);
	}

	return cancel_reservation(head->get_next(), username, phone, celeb_name);
}

bool ArtistScheduler::add_artist(string character_drawn, float raffle_price, char * celeb_name, string & celeb_works, char availability)
{
	int index = get_key(celeb_name);

	if (index < 0)
	{
		cout << "Unable to add Artist. Please try again.\n";
		return false;
	}

	if (!artist_list[index]) // if empty at the index
	{
		artist_list[index] = new ArtistNode(character_drawn, raffle_price, celeb_name, celeb_works, availability);
		return true;
	}

	ArtistNode * temp = artist_list[index];
	artist_list[index] = new ArtistNode(character_drawn, raffle_price, celeb_name, celeb_works, availability);
	artist_list[index]->set_next(temp);
	return true;
}

int ArtistScheduler::display_artists()
{
	if (!artist_list)// empty
		return 0;

	int x = 0;
	for (int i = 0; i < 26; ++i)
	{
		x = x + display_artists(artist_list[i]);
	}

	return x;
		
}

int ArtistScheduler::display_artists(ArtistNode * head) const
{
	int i = 0;

	if (!head) // at the end
		return 1;

	head->display_celebrity();
	head->display_artist_info();
	
	i = 1 + display_artists(head->get_next());
	return i;

}

int ArtistScheduler::remove_all()
{
	if (!artist_list)
		return 0;

	int x = 0;
	for (int i = 0; i < 26; ++i)
	{
		x = x + remove_all(artist_list[i]);
	}

	delete [] artist_list;

	return x;
}

int ArtistScheduler::remove_all(ArtistNode *& head)
{
	int i = 0;

	if (!head)
		return i;

	i = 1 + remove_all(head->get_next());

	delete head;
	return i;
}

// Writer
WriterScheduler::WriterScheduler() // not much to implement?
{}

WriterScheduler::~WriterScheduler() // vectors are freed automatically.
{}

int WriterScheduler::display_writers()
{
	int size = writer_list.size();
	if (size == 0)
	{
		cout << "There are no currently no writers\n";
		return 0;
	}

	int i = 0;
	vector<Writer>::iterator ptr;

	for (ptr = writer_list.begin(); ptr < writer_list.end(); ptr++)
	{
		ptr->display_writer_info();
		i = i + 1;
	}


	/*

	for (i = 0; i < size; ++i)
	{
		writer_list[i].display_writer_info();
	}
	*/

	return i;
}

bool WriterScheduler::find_reservation(string & username, long phone)
{
	int size = writer_list.size();
	if (size == 0)
		return false;

	vector<Writer>::iterator ptr;
	
	for (ptr = writer_list.begin(); ptr < writer_list.end(); ptr++)
	{
		ptr->find_reservation(username, phone);	
	}

	return true;
}

bool WriterScheduler::make_reservation(string & username, long phone, char * celebrity_name)
{	
	int size = writer_list.size();
	if (size == 0)
		return false;

	vector<Writer>::iterator ptr;

	for (ptr = writer_list.begin(); ptr < writer_list.end(); ptr++)
	{
		if (ptr->compare_celebrity(celebrity_name))
		{
			ptr->schedule_writermeet(username, phone);
			return true;
		}
	}

	return false;
}

bool WriterScheduler::cancel_reservation(string & username, long phone, char * celebrity_name)
{	
	int size = writer_list.size();
	if (size == 0)
		return false;

	vector<Writer>::iterator ptr;
	
	for (ptr = writer_list.begin(); ptr < writer_list.end(); ptr++)
	{
		if (ptr->compare_celebrity(celebrity_name))
		{
			ptr->cancel_writermeet(username, phone);
			return true;
		}
	}

	return false;
}

bool WriterScheduler::add_writer(string & book_title, float book_price, char * celeb_name, string & celeb_works, char availability)
{
	writer_list.push_back(Writer(book_title, book_price, celeb_name, celeb_works, availability));

	/*writer_list[0].display_writer_info();

	Writer test(book_title, book_price, celeb_name, celeb_works, availability);

	test.display_writer_info(); // this prints everything properly. Celebrity's data has been set

	writer_list.push_back(test); 
	writer_list[0].display_writer_info(); // data in Celebrity is missing.

	writer_list[0].copy_all(test);
	writer_list[0].display_writer_info(); // data in Celebrity is now copied.
	*/ // Fixing the Celebrity(const Celebrity & to_copy) fixed this issue.



	
	return true;
}

