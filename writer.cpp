#include "celebrity.h"

/*
   Brian Le

   This file contains all the implementations of the Writer and WriterNode class
   */

Writer::Writer(): book("N/A\n"), book_cost(0.00)
{
	/*
	book = "N/A\n";
	book_cost = 0.00;
	*/
}

Writer::Writer(string & book_title, float book_price, char * celeb_name, string & celeb_works, char availability): \
	Celebrity(celeb_name, celeb_works, availability), book("N/A\n"), book_cost(0.00)
{
	book = book_title;
	book_cost = book_price;
}

Writer::~Writer()
{
	book_cost = 0.00;
}


bool Writer::display_writer_info() // displaying the writer's info.
{
	display_celebrity(); // will print their name, works, and availability.

	cout << "Book Written: " << book << endl;
	cout << "Price of a signed " << book << " copy: $" << fixed << setprecision(2) << book_cost << endl;
	return true;
}

bool Writer::schedule_writermeet(const string & user_name, long phone_number)
{
	buy_book(); // ask user if they want to buy a book.
	return schedule_reservation(user_name, phone_number);	
}

bool Writer::cancel_writermeet(const string & user_name, long phone_number)
{
	return cancel_reservation(user_name, phone_number);		
}

bool Writer::buy_book()
{
	char option = 'N';
	float pay = 0.00;

	cout << "Would you like to buy a signed copy of " << book << " for $" << fixed << setprecision(2) << book_cost << "? (Y/N): ";
	cin >> option;
	cin.ignore(100, '\n');

	if (option == 'Y' || option == 'y')
	{
		cout << "Please enter the amount you would like to pay: $";
		cin >> pay;
		cin.ignore(100, '\n');


		if (pay < book_cost)
			cout << "The payment recieved was not sufficient. Please try again at Customer Service\n";

		else
		{
			cout << "Your change is: $" << fixed << setprecision(2) << pay - book_cost << endl;
			return true;
		}
	}

	return false;
}




