#include "celebrity.h"

/*
   Brian Le

   This class contains the Containing class that has a Node of each of the derived classes. Each containing class have separate different
   data structure being implemented.
   */

class ActorScheduler // CLL
{
	public:
		ActorScheduler();
		ActorScheduler(const ActorScheduler & to_copy);	
		ActorScheduler & operator=(const ActorScheduler & src);
		~ActorScheduler();
		int deep_copy(const ActorScheduler & to_copy); 
		int deep_copy2(const ActorScheduler & to_copy); // both deep copies work. I was just experimenting different methods.
		bool find_reservation(string & username, long phone);
		bool make_reservation(string & username, long phone, char * celebrity_name); // should call schedule_selfie();
		bool cancel_reservation(string & username, long phone, char * celebrity_name);
		bool add_actor(char * character, float price, char * celeb_name, string & celeb_works, char availability); 
		int display_actors();
		int remove_all();
		int find_availability(char x) const;

	private:
		int display_actors(ActorNode * head, ActorNode * rear);
		int remove_all(ActorNode *& head);
		bool make_reservation(ActorNode *& head, ActorNode * rear, string & username, long phone, char * celebrity_name);
		bool cancel_reservation(ActorNode *&head, ActorNode * rear, string & username, long phone, char * celebrity_name);
		bool find_reservation(ActorNode * head, ActorNode * rear, string & username, long phone);
		int find_availability(ActorNode * head, ActorNode * read, char x) const;
		int deep_copy(ActorNode *& rear, ActorNode *& head, ActorNode * copy_rear, ActorNode * copy_head);
		int deep_copy2(ActorNode *& head, ActorNode * copy_rear, ActorNode * copy_head);

		ActorNode * actor_list;
};

class ArtistScheduler // Array of LLL
{
	public:
		ArtistScheduler();
		ArtistScheduler(const ArtistScheduler & to_copy);
		ArtistScheduler & operator=(const ArtistScheduler & src);
		~ArtistScheduler();
		bool find_reservation(string & username, long phone);
		bool make_reservation(string & username, long phone, char * celebrity_name); 
		bool cancel_reservation(const string & username, long phone, char * celebrity_name);
		bool add_artist(string character_drawn, float raffle_price, char * celeb_name, string & celeb_works, char availability);
		int get_key(char * celeb_name) const; // returns the ascii value of the celebrity's first initial.
		int display_artists();
		int remove_all();

	private:
		int deep_copy(ArtistNode *& head, ArtistNode * to_copy);
		int display_artists(ArtistNode * head) const;
		int remove_all(ArtistNode *& head);
		bool find_reservation(ArtistNode * head, string & username, long phone);
		bool make_reservation(ArtistNode *& head, string & username, long phone, char * celebrity_name);
		bool cancel_reservation(ArtistNode *& head, const string & username, long phone, char * celeb_name);

		ArtistNode ** artist_list; // a pointer to a pointer of head(s)
		// will become an array of head pointers. We will allocate 26 elements to represent 26 alphabets.
		// Sort the artist nodes based on the first letter of their name
};

class WriterScheduler // STL Vector
{
	public:
		WriterScheduler();
		~WriterScheduler();
		int display_writers();
		bool find_reservation(string & username, long phone);
		bool make_reservation(string & username, long phone, char * celebrity_name);
		bool cancel_reservation(string & username, long phone, char * celebrity_name);
		bool add_writer(string & book_title, float book_price, char * celeb_name, string & celeb_works, char availability);

	private:
		vector<Writer> writer_list;
};
