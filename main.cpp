#include "scheduler.h"
/*
   Brian Le

   This file contains the main program along with other non-member function implementation
   */


void main_menu(); // the menu display
bool get_userinfo(string & name, long & phone); // getting user info
void admin_menu(ActorScheduler & my_actors, ArtistScheduler & my_artists, WriterScheduler & my_writers); // the admin menu only accessbile to certain people.



int main()
{
	// celeb's individual work
	//char character[100];
	string art;
	string book;

	//float price = 0.00;
	char celeb_name[100];
	string work;
	//char day;
	
	string user_name;
	long phone_number = 0; // its a long because ints can't hold 503123456 (it's too big)
	int celeb_type = 0;

	int user_input{0};
	bool program = true;

	ActorScheduler my_actors;
	ArtistScheduler my_artists;
	WriterScheduler my_writers;

	// The main program
	while (program)
	{

		main_menu();
		cin >> user_input;
		cin.ignore(100, '\n');
		cout << '\n';

		switch (user_input)
		{
			/*
			case 1:
				cout << "Enter the actor's name: ";
				cin.get(celeb_name, 100, '\n');
				cin.ignore(100, '\n');

				cout << "Enter a work they were involved in: ";
				getline(cin, work);

				cout << "Enter the name of the Character the Actor played: ";
				cin.get(character, 100, '\n');
				cin.ignore(100, '\n');

				cout << "What day are they available?\n(M - Monday, T - Tuesday, W - Wednesday, R - Thursday, F - Friday, S - Saturday, U - Sunday)" << endl;
				cin >> day;
				cin.ignore(100, '\n');

				cout << "Enter the price of the autograph: ";
				cin >> price;
				cin.ignore(100, '\n');

				my_actors.add_actor(character, price, celeb_name, work, day);
				break;
				
			case 2:
				cout << "Enter the artist's name: ";
				cin.get(celeb_name, 100, '\n');
				cin.ignore(100, '\n');

				cout << "Enter a work they were involved in: ";
				getline(cin, work);

				cout << "Enter the name of the art they have illustrated: ";
				getline(cin, art);

				cout << "What day are they available?\n(M - Monday, T - Tuesday, W - Wednesday, R - Thursday, F - Friday, S - Saturday, U - Sunday)" << endl;
				cin >> day;
				cin.ignore(100, '\n');

				cout << "Enter the price of the raffle tickets: ";
				cin >> price;
				cin.ignore(100, '\n');

				my_artists.add_artist(art, price, celeb_name, work, day);
				break;

			case 3:
				cout << "Enter the writer's name: ";
				cin.get(celeb_name, 100, '\n');
				cin.ignore(100, '\n');

				cout << "Enter a work they were involved in: ";
				getline(cin, work);

				cout << "Enter the name of a book they have written: ";
				getline(cin, book);

				cout << "What day are they available?\n(M - Monday, T - Tuesday, W - Wednesday, R - Thursday, F - Friday, S - Saturday, U - Sunday)" << endl;
				cin >> day;
				cin.ignore(100, '\n');

				cout << "Enter the price of the book: ";
				cin >> price;
				cin.ignore(100, '\n');

				my_writers.add_writer(book, price, celeb_name, work, day);
				break;
				*/

			case 1:
				cout << "Here are the list of participating Actors:\n";
				my_actors.display_actors();
				break;

			case 2:
				cout << "Here are the list of participating Artists:\n";
				my_artists.display_artists();
				break;

			case 3:
				cout << "Here are the list of participating Writers:\n";
				my_writers.display_writers();
				break;

			case 4:
				get_userinfo(user_name, phone_number);

				cout << "Here are the list of participating Actors:\n";
				my_actors.display_actors();

				cout << "Enter an actor's name: ";
				cin.get(celeb_name, 100, '\n');
				cin.ignore(100, '\n');

				my_actors.make_reservation(user_name, phone_number, celeb_name);
				break;

			case 5:
				get_userinfo(user_name, phone_number);

				cout << "Here are the list of participating Artists:\n";
				my_artists.display_artists();

				cout << "Enter the artist's name: ";
				cin.get(celeb_name, 100, '\n');
				cin.ignore(100, '\n');

				my_artists.make_reservation(user_name, phone_number, celeb_name);
				break;
			case 6:
				get_userinfo(user_name, phone_number);

				cout << "Here are the list of participating Writers:\n";
				my_writers.display_writers();

				cout << "Enter the artist's name: ";
				cin.get(celeb_name, 100, '\n');
				cin.ignore(100, '\n');

				my_writers.make_reservation(user_name, phone_number, celeb_name);

				break;

			case 7:
				get_userinfo(user_name, phone_number);

				cout << "Is your appontment with a:\n1) Actor\n2) Artist\n3) Writer\n" << endl;
				cin >> celeb_type;
				cin.ignore(100, '\n');

				if (celeb_type == 1)
				{
					cout << "Here are the list of participating Actors:\n";
					my_actors.display_actors();

					cout << "Enter the actor's name: ";
					cin.get(celeb_name, 100, '\n');
					cin.ignore(100, '\n');

					my_actors.cancel_reservation(user_name, phone_number, celeb_name);
				}

				if (celeb_type == 2)
				{
					cout << "Here are the list of participating Artists:\n";
					my_artists.display_artists();

					cout << "Enter the artist's name: ";
					cin.get(celeb_name, 100, '\n');
					cin.ignore(100, '\n');

					my_artists.cancel_reservation(user_name, phone_number, celeb_name);
				}

				if (celeb_type == 3)
				{
					cout << "Here are the list of participating Writers:\n";
					my_writers.display_writers();

					cout << "Enter the writer's name: ";
					cin.get(celeb_name, 100, '\n');
					cin.ignore(100, '\n');

					my_writers.cancel_reservation(user_name, phone_number, celeb_name);
				}

				break;

			case 8:
				get_userinfo(user_name, phone_number);
				my_actors.find_reservation(user_name, phone_number);
				break;

			case 9:
				get_userinfo(user_name, phone_number);
				my_artists.find_reservation(user_name, phone_number);
				break;

			case 10:
				get_userinfo(user_name, phone_number);
				my_writers.find_reservation(user_name, phone_number);
				break;

				/*
			case 14:
				cout << "(M - Monday, T - Tuesday, W - Wednesday, R - Thursday, F - Friday, S - Saturday, U - Sunday)" << endl;
				cout << "Enter the day you wish to meet with a celebrity: ";
				cin >> day;
				cin.ignore(100, '\n');

				if (my_actors.find_availability(day) <= 0 || )
					cout << "There are no Celebrities available on the specified day\n";
				
				break;
				*/

			case 0:
				program = false;
				break;

			case -1:
				admin_menu(my_actors, my_artists, my_writers);
				break;

			default:
				cout << "Not an option. Try Again." << endl;
				break;
		}
	}



	return 0;
}

void main_menu() // temporary menu. will change to be more practical
{
	cout << "\nWelcome to Comic Con!\n\nPlease select an option:\n";
	/*
	cout << "1) Add Actor\n";
	cout << "2) Add Artist\n";
	cout << "3) Add Writer\n";
	*/
	cout << "1) Show List of All Actors\n";
	cout << "2) Show List of All Artists\n";
	cout << "3) Show List of All Writers\n";
	cout << "4) Schedule a Selfie Session With an Actor\n";
	cout << "5) Schedule a Fan-Meet with an Artist\n";
	cout << "6) Schedule a Autograph Session with a Writer\n";
	cout << "7) Cancel Reservation\n";
	cout << "8) Find Actor Reservation\n";
	cout << "9) Find Artist Reservation\n";
	cout << "10) Find Writer Reservation\n";
//	cout << "14) Find Celebrity by Availability\n";
	cout << "0) Exit\n";
}

bool get_userinfo(string & name, long & phone)
{
		cout << "Enter your name: ";
		getline(cin, name);

		cout << "Enter your 10-digit phone number (no dashes): ";
		cin >> phone;
		cin.ignore(100, '\n');

		return true;
}

void admin_menu(ActorScheduler & my_actors, ArtistScheduler & my_artists, WriterScheduler & my_writers)
{
	// login credentials
	string login = "admin";
	string password = "password";
	
	string username;
	string pass;

	cout << "Enter username: ";
	getline(cin, username);

	cout << "Enter password: ";
	getline(cin, pass);

	if (login != username || password != pass)
	{
		cout << "Invalid Credentials" << endl;
		return;
	}

	// celeb's individual work
	char character[100];
	string art;
	string book;

	float price = 0.00;
	char celeb_name[100];
	string work;
	char day;

	int admin_input;
	

	
	bool admin =  true;

	while (admin)
	{
		cout << "===============================================\n";
		cout << "\t\tADMIN MENU" << endl;
		cout << "===============================================\n";

		cout << "1) Add Actor\n";
		cout << "2) Add Artist\n";
		cout << "3) Add Writer\n";
		cout << "4) Exit Admin Menu\n";

		cin >> admin_input;
		cin.ignore(100, '\n');
		cout << '\n';

		switch (admin_input)
		{
				case 1:
					cout << "Enter the actor's name: ";
					cin.get(celeb_name, 100, '\n');
					cin.ignore(100, '\n');

					cout << "Enter a work they were involved in: ";
					getline(cin, work);

					cout << "Enter the name of the Character the Actor played: ";
					cin.get(character, 100, '\n');
					cin.ignore(100, '\n');

					cout << "What day are they available?\n(M - Monday, T - Tuesday, W - Wednesday, R - Thursday, F - Friday, S - Saturday, U - Sunday)" << endl;
					cin >> day;
					cin.ignore(100, '\n');

					cout << "Enter the price of the autograph: $";
					cin >> price;
					cin.ignore(100, '\n');

					my_actors.add_actor(character, price, celeb_name, work, day);
					break;
					
				case 2:
					cout << "Enter the artist's name: ";
					cin.get(celeb_name, 100, '\n');
					cin.ignore(100, '\n');

					cout << "Enter a work they were involved in: ";
					getline(cin, work);

					cout << "Enter the name of the art they have illustrated: ";
					getline(cin, art);

					cout << "What day are they available?\n(M - Monday, T - Tuesday, W - Wednesday, R - Thursday, F - Friday, S - Saturday, U - Sunday)" << endl;
					cin >> day;
					cin.ignore(100, '\n');

					cout << "Enter the price of the raffle tickets: $";
					cin >> price;
					cin.ignore(100, '\n');

					my_artists.add_artist(art, price, celeb_name, work, day);
					break;

				case 3:
					cout << "Enter the writer's name: ";
					cin.get(celeb_name, 100, '\n');
					cin.ignore(100, '\n');

					cout << "Enter a work they were involved in: ";
					getline(cin, work);

					cout << "Enter the name of a book they have written: ";
					getline(cin, book);

					cout << "What day are they available?\n(M - Monday, T - Tuesday, W - Wednesday, R - Thursday, F - Friday, S - Saturday, U - Sunday)" << endl;
					cin >> day;
					cin.ignore(100, '\n');

					cout << "Enter the price of the book: $";
					cin >> price;
					cin.ignore(100, '\n');

					my_writers.add_writer(book, price, celeb_name, work, day);
					break;

				case 4:
					admin = false;
					break;
					
				default:
					cout << "Not an option. Please try again\n";
					break;
		}
	}


}
