#include <iostream>
#include <cstring>
#include <cctype>
#include <fstream>
#include <string>
#include <vector>
#include <iomanip>
using namespace std;

/* 
   Brian Le

   This header file contains all the classes and their heirarchy, with Celebrity being the base class and
   Artists, Writers, and Actors being derived from teh celebrity class.
   */

class Celebrity
{
	public:
		Celebrity();
		Celebrity(const Celebrity & to_copy);
		Celebrity(char * name, string & works, char day);
		~Celebrity();
		Celebrity & operator=(const Celebrity & src);

		bool display_celebrity(); // show the celebrity's information.
		bool find_reservation(string & user_name, long phone); // for finding the reservation.
		bool compare_user(const string & user_name, long phone); // used to find the user's name
		bool check_availability(char c); // for checking the days the celebrity is available
		bool schedule_reservation(const string & user_name, long phone_number); // for adding the user's name into the 
		bool cancel_reservation(const string & user_name, long phone_number); // for removing a reservation.
		bool compare_celebrity(char * celeb_name); // for finding the celebrity by name.
		bool copy_name(char * string);	
		bool copy_all(const Celebrity & src);
		bool dayoftheweek(char x);

	protected:
		// celebrity info
		char * celebrity_name;
		string celebrity_works;
		char availability;

		// user/fan's info. Each celebrity will hold a list of fans that are meeting with them.
		vector<string> fans_name;
		vector<long>  fans_phone; // the fan's name and fan's phone number should hold the same index everytime it is added.
	
};

class Actor: public Celebrity
{
	public:
		Actor();
		Actor(const Actor & to_copy); // deep copy must be implemented into this copy constructor.
		Actor(char * character, float price, char * celeb_name, string & celeb_works, char availability);
		~Actor();
		Actor & operator=(const Actor & src);

		bool display_actor_info() const; // for showing this actor's info.
		bool schedule_selfie(const string & user_name, long phone_number); // create the reservation.
		bool cancel_selfie(const string & user_name, long phone_number);
		bool buy_autograph();
		//int deep_copy(const Actor & to_copy);

	protected:
		char * acting_character;
		float autograph_cost;
};

class ActorNode: public Actor
{
	public:
		ActorNode();
		ActorNode(const ActorNode & to_copy);
		ActorNode(char * character, float price, char * celeb_name, string & celeb_works, char availability); // for when adding an actor.
		~ActorNode();

		void set_next(ActorNode * new_next);
		ActorNode *& get_next();
		bool has_next() const;

	private:
		ActorNode * next;


};


class Artist: public Celebrity
{
	public:
		Artist();
		Artist(string character_drawn, float raffle_price, char * celeb_name, string & celeb_works, char availability);
		~Artist();


		bool display_artist_info(); // we want to call display_celebrity first before the character works and stuff.
		bool schedule_artistmeet(const string & user_name, long phone_number);
		bool cancel_artistmeet(const string & user_name, long phone_number);
		bool buy_raffle();

	protected:
		string character_work;
		float raffle_cost;
		int winner_raffle; // probably should use a random number generator.

};

class ArtistNode: public Artist
{
	public:
		ArtistNode();
		ArtistNode(const ArtistNode & to_copy);
		ArtistNode(string character_drawn, float raffle_price, char * celeb_name, string & celeb_works, char availability);
		ArtistNode & operator=(const ArtistNode & src);
		~ArtistNode();

		void set_next(ArtistNode * new_next);
		ArtistNode *& get_next();
		bool has_next() const;

	private:
		ArtistNode * next;


};


class Writer: public Celebrity
{
	public:
		Writer();
		Writer(string & book_title, float book_price, char * celeb_name, string & celeb_works, char availability); // constructor to set values.
		~Writer();

		bool display_writer_info();
		bool schedule_writermeet(const string & user_name, long phone_number);
		bool cancel_writermeet(const string & user_name, long phone_number);
		bool buy_book();

	protected:
		string book;
		float book_cost;
};

/*
class WriterNode: public Writer // prolly won't use this. We will use a stl vector instead.
{
	public:
		WriterNode();
		WriterNode(const ActorNode & to_copy);
		WriterNode(string & book_title, float book_price, char * celeb_name, string & celeb_works, char availability);
		~WriterNode();

		void set_next(ActorNode * new_next);
		WriterNode *& get_next();
		bool has_next() const;

	private:
		WriterNode * next;


};
*/
