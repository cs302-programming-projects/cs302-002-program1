#include "celebrity.h"
/* Brian Le

   This file contains the implementations of the ActorNode and Actor class functions
   */


// Actor Nodes
ActorNode::ActorNode(): next(nullptr)
{
	//next = nullptr;
}

ActorNode::ActorNode(const ActorNode & to_copy): Actor(to_copy), next(nullptr) // copy constructor
{
	//next = nullptr;
}

ActorNode::ActorNode(char * character, float price, char * celeb_name, string & celeb_works, char availability): Actor(character, price, celeb_name, celeb_works, availability), next(nullptr)
{
	//next = nullptr;
}

ActorNode::~ActorNode()
{
	next = nullptr;
}

void ActorNode::set_next(ActorNode * new_next)
{
	next = new_next;
}

ActorNode *& ActorNode::get_next()
{
	return next;
}

bool ActorNode::has_next() const
{
	if (next)
		return true;
	else
		return false;
}

// Actor Class
Actor::Actor(): acting_character(nullptr), autograph_cost(0.00)
{
	//autograph_cost = 0.00;
}

Actor::Actor(const Actor & to_copy): Celebrity(to_copy), acting_character(nullptr), autograph_cost(0.00) // copy constructor
{
	if (to_copy.acting_character && !acting_character)
	{
		acting_character = new char[strlen(to_copy.acting_character) + 1];
		strcpy(acting_character, to_copy.acting_character);
	}

	autograph_cost = to_copy.autograph_cost;
}

Actor::Actor(char * character, float price, char * celeb_name, string & celeb_works, char availability): Celebrity(celeb_name, celeb_works, availability),\
														 acting_character(nullptr), autograph_cost(0.00)
{
	if (!acting_character)
	{
		acting_character = new char[strlen(character) + 1];
		strcpy(acting_character, character);
	}

	autograph_cost = price;	
}

Actor::~Actor()
{
	if (acting_character)
		delete[] acting_character;
}

/*
int Actor::deep_copy(const Actor & to_copy) // This is not currently needed atm.
{
	acting_character = new char[strlen(to_copy.acting_character) + 1];
	strcpy(acting_character, to_copy.acting_character);

	autograph_cost = to_copy.autograph_cost;
}
*/

Actor & Actor::operator=(const Actor & src)
{
	if (&src != this)
		return *this;

	delete[] acting_character;
	// must do deep copy
	acting_character = new char[strlen(src.acting_character) + 1];
	strcpy(acting_character, src.acting_character);

	autograph_cost = src.autograph_cost;
	return *this;
}


bool Actor::display_actor_info() const
{
	cout << "Character Acted: " << acting_character << endl;
	cout << "Autograph Cost: $" << autograph_cost << endl << endl;
	return true;
}

bool Actor::schedule_selfie(const string & user_name, long phone_number)
{
	char input;

	// will ask customer to buy autograph or not
	cout << "Would you also like to buy an autograph from " << celebrity_name << " for $" << autograph_cost << "? (Y/N)" << endl;
	cin >> input;
	cin.ignore(100, '\n');

	if (input == 'Y' || input == 'y')
		buy_autograph();

	return schedule_reservation(user_name, phone_number);			
}

bool Actor::cancel_selfie(const string & user_name, long phone_number)
{
	return cancel_reservation(user_name, phone_number);		
}

bool Actor::buy_autograph()
{
	float payment = 0.00;
	cout << "Your total is $" << fixed << setprecision(2) << autograph_cost << endl;
	cout << "Please enter the amount you would like to pay: $";
	cin >> payment;
	cin.ignore(100, '\n');

	if (payment < autograph_cost)
	{
		cout << "The payment recieved was not sufficient. Please try again at Customer Service\n";
		return false;
	}

	else
		cout << "Your change is: $" << fixed << setprecision(2) << payment - autograph_cost << endl;

	return true;
}



