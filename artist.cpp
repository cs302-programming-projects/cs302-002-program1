#include "celebrity.h"
/* Brian Le

   This file contains the implementation of the Artist and ArtistNode member functions
   */

// Artist Node
ArtistNode::ArtistNode(): next(nullptr)
{
	//next = nullptr;		
}

ArtistNode::ArtistNode(const ArtistNode & to_copy): Artist(to_copy), next(nullptr)
{
	//next = nullptr;	
}

ArtistNode::ArtistNode(string character_drawn, float raffle_price, char * celeb_name, string & celeb_works, char availability): \
		Artist(character_drawn, raffle_price, celeb_name, celeb_works, availability), next(nullptr)
{
//	next = nullptr;
}

ArtistNode::~ArtistNode()
{
	next = nullptr;
}


void ArtistNode::set_next(ArtistNode * new_next)
{
	next = new_next;
}

ArtistNode *& ArtistNode::get_next()
{
	return next;
}

bool ArtistNode::has_next() const
{
	if (next)
		return true;
	else
		return false;
}


// Artist
Artist::Artist(): character_work("N/A\n"), raffle_cost(0.00)
{
//	character_work = "N/A\n";
//	raffle_cost = 0.00;
}

Artist::Artist(string character_drawn, float raffle_price, char * celeb_name, string & celeb_works, char availability): \
		Celebrity(celeb_name, celeb_works, availability)
{
	character_work = character_drawn;
	raffle_cost = raffle_price;
}

Artist::~Artist()
{
	raffle_cost = 0.00;
}

bool Artist::display_artist_info()
{
	cout << "Notable Art Work: " << character_work << endl;
	cout << "Raffle Ticket Price: $" << raffle_cost << endl;
	return true;
}

bool Artist::schedule_artistmeet(const string & user_name, long phone_number)
{
	buy_raffle();
	return schedule_reservation(user_name, phone_number);
}

bool Artist::cancel_artistmeet(const string & user_name, long phone_number)
{
	return cancel_reservation(user_name, phone_number);
}

bool Artist::buy_raffle()
{
	char user = 'N';
	float pay = 0.00;

	cout << "Would you like to buy a raffle for $" << fixed << setprecision(2) << raffle_cost << " for a chance to win $100,000 in prizes? (Y/N): ";
	cin >> user;
	cin.ignore(100, '\n');

	if (user == 'y' || user == 'Y')
	{
		cout << "Your total is $" << fixed << setprecision(2) << raffle_cost << endl;
		cout << "Please enter the amount you would like to pay: $";
		cin >> pay;
		cin.ignore(100, '\n');

		if (pay < raffle_cost)
			cout << "The payment recieved was not sufficient. Please try again at Customer Service\n";

		else
		{
			cout << "Your change is: $" << fixed << setprecision(2) << pay - raffle_cost << endl;
			return true;
		}
	}
	return false;
}


